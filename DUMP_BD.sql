-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2018 at 11:19 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `intelirec`
--

-- --------------------------------------------------------

--
-- Table structure for table `receita`
--

CREATE TABLE `receita` (
  `id` varchar(32) NOT NULL,
  `medico_crm` varchar(20) NOT NULL,
  `farma_crf` varchar(20) NOT NULL,
  `farma_cod` int(10) NOT NULL,
  `paciente_address` varchar(100) NOT NULL,
  `paciente_cpf` varchar(14) NOT NULL,
  `paciente_nome` varchar(100) NOT NULL,
  `data` date NOT NULL,
  `medicamentos` text NOT NULL,
  `paciente_rg` varchar(14) NOT NULL,
  `status` varchar(20) NOT NULL,
  `blockchain_hash` varchar(100) NOT NULL,
  `medico_nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receita`
--

INSERT INTO `receita` (`id`, `medico_crm`, `farma_crf`, `farma_cod`, `paciente_address`, `paciente_cpf`, `paciente_nome`, `data`, `medicamentos`, `paciente_rg`, `status`, `blockchain_hash`, `medico_nome`) VALUES
('0.15583840661109338', '123456', '0', 0, 'Terra alta', '787382738', 'Carlos José', '2018-10-31', '{\"nome\":\"RESPIRAN - 5 MG/ML XPE CT FR PLAS AMB X 120 ML + CP MED\",\"instrucoes\":\"4 x ao dia\",\"cod\":\"517609501131410\"},{\"nome\":\"EXPECDILAT - 5 MG/ML XPE CT FR VD AMB X 120 ML + CP MED\",\"instrucoes\":\"4 x ao dia\",\"cod\":\"531618502132416\"}', '787827387', 'Proccessed', '', ''),
('0.4097145517673846', '123456', '0', 0, 'Carlos Daux', '78718781722', 'Luis Ismael', '2018-10-31', '{\"nome\":\"BUSONID - 200 MCG CAP GEL DURA PO INAL CT BL AL/PVDC X 60\",\"instrucoes\":\"3 x ao dia\",\"cod\":\"521102908111415\"},{\"nome\":\"ACECLO-GRAN - 100 MG COM REV CT BL AL AL X 12\",\"instrucoes\":\"4 x ao dia\",\"cod\":\"538813080046904\"}', '878738273', 'Proccessed', '', ''),
('0.9786658218551167', '123456', '0', 0, 'Restaurante', '89898981', 'Luis Ismael', '2018-10-31', '{\"nome\":\"RIVOTRIL - 0,25 MG COM SUB CT BL AL PLAST INC X 30\",\"instrucoes\":\"Meia dose a cada 1 semana\",\"cod\":\"529204807115314\"},{\"nome\":\"TOXÓIDE TETÂNICO - SUS INJ CT 50 AMP VD INC X 0,5 ML\",\"instrucoes\":\"4 x ao dia\",\"cod\":\"545316010000904\"},{\"nome\":\"BUSONID - 200 MCG CAP GEL DURA PO INAL CT BL AL/PVDC X 60\",\"instrucoes\":\"4  ao dia\",\"cod\":\"521102908111415\"}', '989898989', 'Invalidated', '', ''),
('c4ca4238a0b923820dcc509a6f758495', '123', '1234', 1, 'Alvaro Dias', '00988866654', 'José alencar', '2018-10-28', '{\"nome\":\"BUSCOMPAN\",\"instrucoes\":\"3X ao dia\",\"cod\":\"1\"},{\"nome\":\"BUSCOMPAN COMPOSTO\",\"instrucoes\":\"3X ao dia\",\"cod\":\"2\"}', '8888888', 'PRESCRITA', '', ''),
('c4ca4238a0b923820dcc509a6f75849b', '123', '1234', 1, 'Rua Pio X', '99988876543', 'Paulo Santos', '2018-10-28', '{\"nome\":\"BUSCOMPAN\",\"instrucoes\":\"3X ao dia\",\"cod\":\"1\"},{\"nome\":\"BUSCOMPAN COMPOSTO\",\"instrucoes\":\"3X ao dia\",\"cod\":\"2\"}', '555444', 'PRESCRITA', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `regra`
--

CREATE TABLE `regra` (
  `id` int(11) NOT NULL,
  `crm` varchar(20) DEFAULT NULL,
  `medico` varchar(150) DEFAULT NULL,
  `medicamento` varchar(150) DEFAULT NULL,
  `medicamento_id` varchar(20) DEFAULT NULL,
  `regra_type` varchar(20) NOT NULL,
  `blockchain_hash` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regra`
--

INSERT INTO `regra` (`id`, `crm`, `medico`, `medicamento`, `medicamento_id`, `regra_type`, `blockchain_hash`) VALUES
(8511, '', '', 'ACEBROFILINA - 10 MG/ ML XPE CX 50 FR PLAS AMB X 120 ML + 50 CP MED (EMB HOSP)', '528525701135113', 'REMED_PROIBIDO', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `email` varchar(60) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `blockconnection` text NOT NULL,
  `crm` varchar(20) NOT NULL,
  `crf` varchar(20) NOT NULL,
  `farma_cod` int(10) NOT NULL,
  `address` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `name`, `pass`, `blockconnection`, `crm`, `crf`, `farma_cod`, `address`, `type`) VALUES
('anvisa@teste.com', 'Anvisa Admin', '12345', '', '', '', 0, '', 'ANVISA'),
('drjoao@teste.com', 'Dr João', '12345', '', '123456', '', 0, 'Av José Carlos Daux', 'MED'),
('farmabem@teste.com', 'Farmabem', '12345', '', '', '12345', 1, 'Rua Pio X', 'FARMA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `receita`
--
ALTER TABLE `receita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regra`
--
ALTER TABLE `regra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);
