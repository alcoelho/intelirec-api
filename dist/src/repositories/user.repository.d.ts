import { DefaultCrudRepository } from '@loopback/repository';
import { MysqlconectDataSource } from '../datasources';
import { User } from '../models';
export declare class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.email> {
    constructor(dataSource: MysqlconectDataSource);
}
