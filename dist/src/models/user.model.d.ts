import { Entity } from '@loopback/repository';
export declare class User extends Entity {
    name: string;
    pass: string;
    email: string;
    blockconnection?: string;
    crm?: string;
    farma_cod?: number;
    address?: string;
    crf?: string;
    constructor(data?: Partial<User>);
}
