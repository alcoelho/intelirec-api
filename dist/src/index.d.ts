import { IntelirecApiApplication } from './application';
import { ApplicationConfig } from '@loopback/core';
export { IntelirecApiApplication };
export declare function main(options?: ApplicationConfig): Promise<IntelirecApiApplication>;
