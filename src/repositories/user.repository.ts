import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';

import {MysqlconectDataSource} from '../datasources';
import {User} from '../models';

export class UserRepository extends
    DefaultCrudRepository<User, typeof User.prototype.email> {
  constructor(
      @inject('datasources.mysqlconect') dataSource: MysqlconectDataSource,
  ) {
    super(User, dataSource);
  }
}
