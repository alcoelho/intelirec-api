import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';

import {MysqlconectDataSource} from '../datasources';
import {Receita} from '../models';

export class ReceitaRepository extends DefaultCrudRepository<
  Receita,
  typeof Receita.prototype.id
> {
  constructor(
    @inject('datasources.mysqlconect') dataSource: MysqlconectDataSource,
  ) {
    super(Receita, dataSource);
  }
}
