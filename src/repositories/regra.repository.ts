import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Regra} from '../models';
import {MysqlconectDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RegraRepository extends DefaultCrudRepository<
  Regra,
  typeof Regra.prototype.id
> {
  constructor(
    @inject('datasources.mysqlconect') dataSource: MysqlconectDataSource,
  ) {
    super(Regra, dataSource);
  }
}
