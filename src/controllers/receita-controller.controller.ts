import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {Receita} from '../models';
import {ReceitaRepository} from '../repositories';

export class ReceitaControllerController {
  constructor(
    @repository(ReceitaRepository)
    public receitaRepository : ReceitaRepository,
  ) {}

  @post('/receitas', {
    responses: {
      '200': {
        description: 'Receita model instance',
        content: {'application/json': {'x-ts-type': Receita}},
      },
    },
  })
  async create(@requestBody() receita: Receita): Promise<Receita> {
    return await this.receitaRepository.create(receita);
  }

  @get('/receitas/count', {
    responses: {
      '200': {
        description: 'Receita model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Receita)) where?: Where,
  ): Promise<Count> {
    return await this.receitaRepository.count(where);
  }

  @get('/receitas', {
    responses: {
      '200': {
        description: 'Array of Receita model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Receita}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Receita)) filter?: Filter,
  ): Promise<Receita[]> {
    return await this.receitaRepository.find(filter);
  }

  @patch('/receitas', {
    responses: {
      '200': {
        description: 'Receita PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() receita: Receita,
    @param.query.object('where', getWhereSchemaFor(Receita)) where?: Where,
  ): Promise<Count> {
    return await this.receitaRepository.updateAll(receita, where);
  }

  @get('/receitas/{id}', {
    responses: {
      '200': {
        description: 'Receita model instance',
        content: {'application/json': {'x-ts-type': Receita}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Receita> {
    return await this.receitaRepository.findById(id);
  }

  @patch('/receitas/{id}', {
    responses: {
      '204': {
        description: 'Receita PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody() receita: Receita,
  ): Promise<void> {
    await this.receitaRepository.updateById(id, receita);
  }

  @del('/receitas/{id}', {
    responses: {
      '204': {
        description: 'Receita DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.receitaRepository.deleteById(id);
  }
}
