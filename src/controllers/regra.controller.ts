import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {Regra} from '../models';
import {RegraRepository} from '../repositories';

export class RegraController {
  constructor(
    @repository(RegraRepository)
    public regraRepository : RegraRepository,
  ) {}

  @post('/regras', {
    responses: {
      '200': {
        description: 'Regra model instance',
        content: {'application/json': {'x-ts-type': Regra}},
      },
    },
  })
  async create(@requestBody() regra: Regra): Promise<Regra> {
    return await this.regraRepository.create(regra);
  }

  @get('/regras/count', {
    responses: {
      '200': {
        description: 'Regra model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Regra)) where?: Where,
  ): Promise<Count> {
    return await this.regraRepository.count(where);
  }

  @get('/regras', {
    responses: {
      '200': {
        description: 'Array of Regra model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Regra}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Regra)) filter?: Filter,
  ): Promise<Regra[]> {
    return await this.regraRepository.find(filter);
  }

  @patch('/regras', {
    responses: {
      '200': {
        description: 'Regra PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() regra: Regra,
    @param.query.object('where', getWhereSchemaFor(Regra)) where?: Where,
  ): Promise<Count> {
    return await this.regraRepository.updateAll(regra, where);
  }

  @get('/regras/{id}', {
    responses: {
      '200': {
        description: 'Regra model instance',
        content: {'application/json': {'x-ts-type': Regra}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Regra> {
    return await this.regraRepository.findById(id);
  }

  @patch('/regras/{id}', {
    responses: {
      '204': {
        description: 'Regra PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() regra: Regra,
  ): Promise<void> {
    await this.regraRepository.updateById(id, regra);
  }

  @del('/regras/{id}', {
    responses: {
      '204': {
        description: 'Regra DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.regraRepository.deleteById(id);
  }
}
