import {Entity, model, property} from '@loopback/repository';

@model()
export class Regra extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
  })
  crm?: string;

  @property({
    type: 'string',
  })
  medico?: string;

  @property({
    type: 'string',
  })
  medicamento?: string;

  @property({
    type: 'string',
  })
  medicamento_id?: string;

  @property({
    type: 'string',
  })
  regra_type: string;

  @property({
    type: 'string',
  })
  blockchain_hash: string;

  constructor(data?: Partial<Regra>) {
    super(data);
  }
}
