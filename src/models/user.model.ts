import {Entity, Model, model, property} from '@loopback/repository';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({type: 'string', required: true})
  pass: string;

  @property({
    type: 'string',
    required: true,
    id: true,
  })
  email: string;

  @property({
    type: 'string',
  })
  blockconnection?: string;

  @property({
    type: 'string',
  })
  crm?: string;

  @property({
    type: 'number',
  })
  farma_cod?: number;

  @property({
    type: 'string',
  })
  address?: string;

  @property({
    type: 'string',
  })
  crf?: string;

  @property({
    type: 'string',
  })
  type: string;

  constructor(data?: Partial<User>) {
    super(data);
  }
}
