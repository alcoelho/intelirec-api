import {Entity, model, property} from '@loopback/repository';

@model()
export class Receita extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  medico_crm: string;

  @property({
    type: 'string',
    required: true,
  })
  medico_nome: string;

  @property({
    type: 'string',
    required: false,
  })
  farma_crf?: string;

  @property({
    type: 'string',
    required: false,
  })
  farmaceutico_nome?: string;

  @property({
    type: 'string',
    required: false,
  })
  farma_cod?: string;

  @property({
    type: 'string',
    required: true,
  })
  paciente_address: string;

  @property({
    type: 'string',
    required: true,
  })
  paciente_cpf: string;

  @property({
    type: 'date',
    required: true,
  })
  data: string;

  @property({
    type: 'string',
    required: true,
  })
  medicamentos: string;

  @property({
    type: 'string',
    required: true,
  })
  paciente_rg: string;

  @property({
    type: 'string',
    required: true,
  })
  paciente_nome: string;

  @property({
    type: 'string',
    required: true,
  })
  status: string;

  @property({
    type: 'string',
    required: true,
  })
  blockchain_hash: string;

  @property({
    type: 'string',
    required: false,
  })
  status_hash?: string;

  constructor(data?: Partial<Receita>) {
    super(data);
  }
}
