import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './mysqlconect.datasource.json';

export class MysqlconectDataSource extends juggler.DataSource {
  static dataSourceName = 'mysqlconect';

  constructor(
    @inject('datasources.config.mysqlconect', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
